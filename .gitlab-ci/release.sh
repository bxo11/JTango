#!/bin/bash

set -e
ROOT="${PWD}"

# This script should be run from JTango root directory (.gitlab-ci/release.sh)
function parent_dir() {
    cd parent
}

function working_dir() {
    cd "${ROOT}"
}

function exit_if_null() {
    if [ -z "$1" ]
    then
      echo "$2"
      exit 1
    fi
}

function get_inputs() {
  exit_if_null "$1" "No versions supplied"
  exit_if_null "$2" "No remote URL supplied"

  RELEASE_VERSION=$1
  REMOTE_URL=$2
}

get_inputs "$@"

cp .gitlab-ci/settings.xml "${HOME}"/.m2/settings.xml

## update to RELEASE(TAG) VERSION
parent_dir
mvn versions:set versions:update-child-modules -DnewVersion="${RELEASE_VERSION}" -DprocessAllModule -DgenerateBackupPoms=false -Drelease

## deploy artifacts
working_dir
mvn deploy -Drelease

## update to next SNAPSHOT VERSION
parent_dir
mvn versions:set versions:update-child-modules -DprocessAllModules -DnextSnapshot -DgenerateBackupPoms=false -Drelease

working_dir
# example url: https://JTango_access_token:"${GITLAB_TOKEN}"@gitlab.com/tango-controls/JTango.git
.gitlab-ci/push.sh "$REMOTE_URL"
